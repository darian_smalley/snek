﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayingField : MonoBehaviour {
	public float width = 20;
	public float height = 20;
	public static int initLen = 4;
	public GameObject food;
	public List<GameObject> snek = new List<GameObject>(initLen);
	public int speed = 1;
	public float previousTime = 0;
	public static Vector3 momentum = new Vector3(0,0);

	void Start () {
		// Set up camera
		snek.Clear();
		Camera mainCam = Camera.main;
		mainCam.transform.position = new Vector3(width/2,height/2, -10);		
		mainCam.orthographicSize = height/2;
		mainCam.aspect = 1;

		// Create playing field tiles
		GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
		plane.transform.position = new Vector3(width/2,height/2,0);
		plane.transform.localScale = new Vector3(width/10, height/10, width/10);	
		plane.transform.Rotate(new Vector3((90+180), 0, 0));
		var mat = new Material(Shader.Find("Diffuse"));
		mat.color = Color.black;
		plane.GetComponent<Renderer>().material = mat;

		// Spawn Snek
		var x = Mathf.Floor(UnityEngine.Random.Range(1,width));
		
		for ( int i = 0; i<initLen; i++) {
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.position = new Vector2(x+i,x);
			mat = new Material(Shader.Find("Diffuse"));
			mat.color = Color.white;
			cube.GetComponent<Renderer>().material = mat;
			snek.Add(cube);
		}

		// Spawn Food
		var foodSpawn = Mathf.Floor(UnityEngine.Random.Range(1,width));
		food = GameObject.CreatePrimitive(PrimitiveType.Cube);
		food.transform.position = new Vector2(foodSpawn,foodSpawn);
		mat = new Material(Shader.Find("Diffuse"));
		mat.color = Color.green;
		food.GetComponent<Renderer>().material = mat;

		// Stat Moving Snek
		InvokeRepeating("MoveSnek", 0, 0.1f);
	}
	
	void Update () {
		// Check User Input
		var horizontal = Input.GetAxis("Horizontal");
		var vertical = Input.GetAxis("Vertical");

		if (horizontal > 0) {
			momentum = new Vector3(1,0);
		} 
		else if (horizontal < 0) {
			momentum = new Vector3(-1,0);
		} 
		else if ( vertical > 0) {
			momentum = new Vector3(0,1);
		}
		else if ( vertical < 0) {
			momentum = new Vector3(0,-1);
		}

		// Check if Snek ate
		if (snek[0].transform.position == food.transform.position) {
			// Add cube to snek
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.position = new Vector3(snek[snek.Count -1].transform.position.x,snek[snek.Count -1].transform.position.y);
			var mat = new Material(Shader.Find("Diffuse"));
			mat.color = Color.white;
			cube.GetComponent<Renderer>().material = mat;
			snek.Add(cube);

			// Respawn food
			var foodSpawn = Mathf.Floor(UnityEngine.Random.Range(1,width));
			food.transform.position = new Vector3(foodSpawn, foodSpawn);
		}
	}

    private void MoveSnek()
    {	
		if ( momentum.magnitude == 0) 
			return;

		for(int i = snek.Count-1; i>0; i--) {
			snek[i].transform.position = snek[i-1].transform.position;

			if (snek[i].transform.position == snek[0].transform.position + momentum || isOutOfBounds(snek[0].transform.position)) {
				snek.Clear();
				Application.Quit();
				UnityEditor.EditorApplication.isPlaying = false;
			}
		}

		snek[0].transform.position = snek[0].transform.position + momentum;
    }

    private bool isOutOfBounds(Vector3 position)
    {
        return position.x > width || position.x < 0 || position.y > height || position.y < 0;
    }
}
