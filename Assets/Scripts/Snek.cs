﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snek : MonoBehaviour {
	public static int initLen = 4;
	public GameObject[] snek = new GameObject[initLen];
	public float width = 40;
	public float height = 40;
	// Use this for initialization
	void Start () {
		var x = Mathf.Floor(Random.Range(0,width));
		
		for ( int i = 0; i<initLen; i++) {
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.position = new Vector2(x+i,x);
			var mat = new Material(Shader.Find("Diffuse"));
			mat.color = Color.white;
			cube.GetComponent<Renderer>().material = mat;
		}	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
